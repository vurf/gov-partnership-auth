package com.rgsu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GovPartnershipAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(GovPartnershipAuthApplication.class, args);
	}
}
